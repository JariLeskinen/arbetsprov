'use strict';

var db = {};
var express = require('express');
var bodyParser = require('body-parser');
var dbStore = require('./db')(db);
var json2xml = require('json2xml');
var pd = require('pretty-data').pd;
var libxml = require('libxmljs');
var fs = require('fs');
var app = express();

app.use(bodyParser.json()); // Middleware that parses http request body and populates the express body property.

/*
  Define api endpoints for the system.
*/

// List all programs.
app.route('/api/program').get(function(req, res) {
  db.programs.find({}).exec(function(err, docs) {
    if (err) { res.status(400).send('Error'); return; }

    res.json({result: docs});
  });
});

// Fetch a program.
app.route('/api/program/:programId').get(function(req, res) {
  var query = {};
  query._id = req.params.programId;

  db.programs.find(query).exec(function(err, docs) {
    if (err) { res.status(400).send('Error'); return; }

    res.json({result: docs});
  });
});

// Create a new program.
app.route('/api/program').post(function(req, res) {
  var program = req.body.program;

  db.programs.insert(program, function(err, docs) {
    if (err) { res.status(400).send('Error'); return; }

    res.json(docs);
  });
});

// Update existing program.
app.route('/api/program/:programId').put(function(req, res) {
  var program = req.body.program;
  var query = {};
  query._id = req.params.programId;

  db.programs.update(query, program, {}, function(err, docs) {
    if (err) { res.status(400).send('Error'); return; }

    res.json({success: true});
  });
});

// Delete a program.
app.route('/api/program/:programId').delete(function(req, res) {
  var query = {};
  query._id = req.params.programId;

  db.programs.remove(query, {}, function(err, numRemoved) {
    if (err) { res.status(400).send('Error'); return; }

    res.json({success: true});
  });
});

// List all programs in XML format.
app.get('/programsXml', function(req, res) {
  // Read in and parse the xml validation file.
  var data = fs.readFileSync('xsd.xml', {encoding: 'utf8'});
  var xsdDoc = libxml.parseXml(data);

  db.programs.find({}).exec(function(err, docs) {
    if (err) { res.status(400).send('Error'); return; }

    var programs = [];

    for (var i = 0; i < docs.length; ++i) {
      var doc = docs[i];
      delete doc._id;
      programs.push({program: doc});
    }

    var programsXmlText = json2xml({programs: programs});
    var programsXmlDoc = libxml.parseXml(programsXmlText);

    if (!programsXmlDoc.validate(xsdDoc)) {
      res.send('Error, XML document didn\'t validate.');
      return;
    }

    res.set('Content-Type', 'text/xml');
    res.send(pd.xml(programsXmlText));
  });
});



// Serve the client directory on root request.
app.use('/', express.static(__dirname + '/client'))

var server = app.listen(3000, function () {
  console.log('Server listening at http://%s:%s', server.address().address, server.address().port);
});
