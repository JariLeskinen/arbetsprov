'use strict';

// Create/load the database.

var Datastore = require('nedb');

module.exports = function(db) {
  db.programs = new Datastore('db/programs.db');
  db.programs.loadDatabase();

  return Datastore;
};