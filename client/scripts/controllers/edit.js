'use strict';

angular.module('clientApp')
  .controller('EditCtrl', function ($scope, $route, $http) {
    var programId = $route.current.params.programId;
    $scope.type = programId ? 'edit' : 'create';

    if ($scope.type === 'edit') {
      $http.get('/api/program/' + programId).
        success(function(data) {
          console.log(data);
          $scope.program = data.result[0];
        }).
        error(function() {
          console.log('Couldn\'t fetch program with id:', programId);
        });
    }

    $scope.submit = function() {
      if ($scope.type === 'edit') {
        updateProgram();
      } else {
        createProgram();
      }
    };

    var updateProgram = function() {
      $http.put('/api/program/' + programId, {program: $scope.program}).
        success(function(data) {
        }).
        error(function() {
          console.log('Couldn\'t update program with id:', programId);
        });
    };

    var createProgram = function() {
     $http.post('/api/program', {program: $scope.program}).
        success(function(data) {
        }).
        error(function() {
          console.log('Couldn\'t create new program.');
        });
    };
  });