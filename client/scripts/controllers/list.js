'use strict';

angular.module('clientApp')
  .controller('ListCtrl', function ($scope, $http) {
    // Fetch programs from server.
    $http.get('/api/program').
      success(function(data) {
        $scope.programs = data.result;
      }).
      error(function() {
        console.log('Couldn\'t fetch programs.');
      });

    $scope.deleteProgram = function(id) {
      $http.delete('/api/program/' + id).
        success(function(){
          for (var i = 0; i < $scope.programs.length; ++i) {
            var program = $scope.programs[i];

            if (program._id === id) {
              $scope.programs.splice(i, 1);
              break;
            }
          }
        }).
        error(function() {
          console.log('Couldn\'t delete program with id:', id);
        });
    };
  });
